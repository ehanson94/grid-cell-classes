class TicTacToeGrid extends GenericGrid {

    constructor (options) {
        super(options)
        this.playerTurn = 'black'
    }

    togglePlayerTurn () {
        this.playerTurn = (this.playerTurn === 'black')
        ? 'red'
        : 'black'
    }

    createCell () {
        return new TicTacToeCell(this.options)
    }

    registerPlayerOnCell (event) {
        const cellElement = event.target
        const cell = event.target.instance
        cell.registerPlayerOwnership(this.playerTurn)

        this.togglePlayerTurn()
    }

}