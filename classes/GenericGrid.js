class GenericGrid {

    constructor (options = {
        rowCount: 10,
        columnCount: 10,
        cellWidth: '10px',
        cellHeight: '10px',
    }) {
        this.options = options
        this.cellType = options.cellType || GenericCell
        this.matrix = []
    }

    createGridElement () {
        this.element = document.createElement('div')
        this.element.id = 'grid'
        for (let rowIndex = 0; rowIndex < this.options.rowCount; rowIndex++) {
            const rowElement = this.createRowElement(rowIndex)
            this.element.appendChild(rowElement)
            this.matrix[rowIndex] = []

            for (let cellIndex = 0; cellIndex < this.options.columnCount; cellIndex++) {
                const cell = this.createCell()
                rowElement.appendChild(cell.createCellElement(rowIndex, cellIndex))
                this.matrix[rowIndex][cellIndex] = cell
            }

        }
        return this.element
    }

    createCell () {
        return new GenericCell(this.options)
    }

    createRowElement (rowIndex) {
        const element = document.createElement('div')
        element.classList.add('row')
        element.dataset.rowIndex = rowIndex

        return element
    }

    addEventListeners (eventDescriptions) {
        for (let eventDescription of eventDescriptions) {
            console.log(eventDescription)

            const { listener, type, callback } = eventDescription
            listener.addEventListener(type, callback)
        }
    }

}
