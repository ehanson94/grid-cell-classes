class EventDescription {

    constructor ({ listener, type, callback, context = null }) {
        this.listener = listener
        this.type = type
        this.callback = context ? callback.bind(context) : callback
    }

}