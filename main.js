const gridContainer = document.querySelector('#grid-container')

const grid = new TicTacToeGrid({
    rowCount: 3,
    columnCount: 3,
    cellWidth: '120px',
    cellHeight: '120px',
    cellClasses: ['red'],
    cellType: TicTacToeCell,
})

const gridElement = grid.createGridElement()
gridContainer.appendChild(gridElement)

const eventListeners = [
    new EventDescription({
        usage: `Registers player when clicking on a cell`,
        listener: gridElement,
        type: 'click',
        callback: grid.registerPlayerOnCell,
        context: grid,
    })
  ]

grid.addEventListeners(eventListeners)